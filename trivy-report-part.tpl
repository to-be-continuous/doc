{{- $critical := 0 }}
{{- $high := 0 }}
{{- $medium := 0 }}
{{- $low := 0 }}
{{- $unknown := 0 }}
{{- range . }}
{{- range .Vulnerabilities }}
{{- if  eq .Severity "CRITICAL" }}
{{- $critical = add $critical 1 }}
{{- end }}
{{- if  eq .Severity "HIGH" }}
{{- $high = add $high 1 }}
{{- end }}
{{- if  eq .Severity "MEDIUM" }}
{{- $medium = add $medium 1 }}
{{- end }}
{{- if  eq .Severity "LOW" }}
{{- $low = add $low 1 }}
{{- end }}
{{- if  eq .Severity "UNKNOWN" }}
{{- $unknown = add $unknown 1 }}
{{- end }}
{{- end }}
{{- end }}
{{- $max := "" }}
{{- if $critical }}
{{- $max = "CRITICAL" }}
{{- else if $high }}
{{- $max = "HIGH" }}
{{- else if $medium }}
{{- $max = "MEDIUM" }}
{{- else if $low }}
{{- $max = "LOW" }}
{{- else if $unknown }}
{{- $max = "UNKNOWN" }}
{{- end }}
{{- $next := 0 }}
<td class="trivy vuln severity-{{ $max }}">
{{- if $critical }}
{{- $next = add $high $medium $low $unknown }}
{{ $critical }} Critical{{ if $next }},{{ end }}
{{- end }}
{{- if $high }}
{{- $next = add $medium $low $unknown }}
{{ $high }} High{{ if $next }},{{ end }}
{{- end }}
{{- if $medium }}
{{- $next = add $low $unknown }}
{{ $medium }} Medium{{ if $next }},{{ end }}
{{- end }}
{{- if $low }}
{{- $next = $unknown }}
{{ $low }} Low{{ if $next }},{{ end }}
{{- end }}
{{- if $unknown }}
{{ $unknown }} Unknown
{{- end }}
</td>

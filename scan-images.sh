#!/usr/bin/env sh
# =========================================================================================
# Copyright (C) 2021 Orange & contributors
#
# This program is free software; you can redistribute it and/or modify it under the terms
# of the GNU Lesser General Public License as published by the Free Software Foundation;
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with this
# program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA  02110-1301, USA.
# =========================================================================================

set -e

function log_info() {
  echo -e "[\\e[1;94mINFO\\e[0m] $*"
}

function log_warn() {
  echo -e "[\\e[1;93mWARN\\e[0m] $*"
}

function log_error() {
  echo -e "[\\e[1;91mERROR\\e[0m] $*"
}

function fail() {
  log_error "$@"
  exit 1
}

mkdir -p -m 777 reports
mkdir -p public/secu

while read -r line
do
  tmpl_name=$(echo "$line" | cut -d '|' -f1)
  img_usage=$(echo "$line" | cut -d '|' -f2)
  var_name=$(echo "$line" | cut -d '|' -f3)
  img_uri=$(echo "$line" | cut -d '|' -f4)
  log_info "--- scanning ($img_usage) \\e[33;1m${var_name}\\e[0m image for \\e[33;1m${tmpl_name}\\e[0m template: \\e[32m${img_uri}\\e[0m"
  # JSON format
  trivy image --cache-dir .cache --scanners vuln --format json --exit-code 0 --output "docs/secu/trivy-${var_name}.json" "$img_uri" || log_warn "Failed"
  # MkDocs format
  trivy convert --format template --template "@trivy-report.tpl" --output "docs/secu/trivy-${var_name}.md" "docs/secu/trivy-${var_name}.json" || log_warn "Failed"
  # HTML part
  trivy convert --format template --template "@trivy-report-part.tpl" --output "docs/secu/trivy-${var_name}.part.html" "docs/secu/trivy-${var_name}.json" || log_warn "Failed"
  # # GitLab format
  # trivy convert --format template --template "@/contrib/gitlab.tpl" --output "reports/trivy-${var_name}.gitlab.json" "docs/secu/trivy-${var_name}.json" || log_warn "failed"
  # # text format (stdout)
  # trivy convert --format table "docs/secu/trivy-${var_name}.json" || log_warn "failed"
  # clear JSON format
  rm "docs/secu/trivy-${var_name}.json"
done < ./tbc-default-images.out
